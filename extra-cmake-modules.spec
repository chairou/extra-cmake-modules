%global majmin %majmin_ver_kf5
%global stable %stable_kf5

Summary: Additional modules for CMake build system
Name:    extra-cmake-modules
Version: 5.110.0
Release: 1%{?dist}
License: BSD
URL:     https://api.kde.org/ecm/
Source0:   http://download.kde.org/%{stable}/frameworks/%{majmin}/%{name}-%{version}.tar.xz
Patch3000: extra-cmake-modules-5.39.0-poppler_overlinking.patch
Patch3001: extra-cmake-modules-5.89.0-qt_prefix.patch
BuildArch: noarch

BuildRequires: kf5-rpm-macros
BuildRequires: make
BuildRequires: qt5-qttools-devel
BuildRequires: python3-sphinx

Requires: kf5-rpm-macros
Recommends: appstream

%description
Additional modules for CMake build system needed by KDE Frameworks.



%prep
%autosetup -p1



%build
%cmake_kf5 \
  -DBUILD_HTML_DOCS:BOOL=ON \
  -DBUILD_MAN_DOCS:BOOL=ON \
  -DBUILD_TESTING:BOOL=ON \
  -DSphinx_BUILD_EXECUTABLE:PATH=%{_bindir}/sphinx-build-3

%cmake_build



%install
%cmake_install



%check
export CTEST_OUTPUT_ON_FAILURE=1
make test ARGS="--output-on-failure --timeout 300" -C %{_target_platform} ||:



%files
%license LICENSES/*.txt
%doc README.rst
%{_datadir}/ECM/
%{_kf5_docdir}/ECM/html/
%{_kf5_mandir}/man7/ecm*.7*



%changelog
* Sun Oct 22 2023 Chair <chairou@tencent.com> - 5.110.0-1
- initial
